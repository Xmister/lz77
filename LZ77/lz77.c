#include "lz77.h"

void help(void) {
	printf("Usage: lz77 [options] input output\n\
	Options:\n\
	\t--hk value:int\tSet Hk to value\n\
	\t--he value:int\tSet He to value\n\
	\t--prefetch Preload input to memory\n");
}

int cmdline(int ac, char** args, int* hk, int* he, char** input, char** output, char* prefetch, char* act) {
	int i, ret=0;
	char* cmd = NULL;
	for (i = 0; i < ac; i++) {
		if (strstr(args[i], "--") != NULL) {
			cmd = args[i] + 2;
			if (!strcmp(cmd, "help")) {
				help();
				return 1;
			}
			else if (!strcmp(cmd, "prefetch")) {
				*prefetch = 1;
				cmd = NULL;
			}
			else if (!strcmp(cmd, "compress")) {
				*act=2;
				cmd = NULL;
			}
			else if (!strcmp(cmd, "decompress")) {
				*act=3;
				cmd = NULL;
			}
		}
		else if (cmd != NULL) {
			if (!strcmp(cmd, "hk")) *hk = strtol(args[i], NULL, 10);
			else if (!strcmp(cmd, "he")) *he = strtol(args[i], NULL, 10);
			cmd = NULL;
		}
		else if (i > 0) {
			if (*(input)[0] == '\0') strcpy(*input, args[i]);
			else strcpy(*output, args[i]);
		}
	}
	if (*(input)[0] == '\0')
	{
		printf("No input file given! For help use --help parameter.\n");
		return -1;
	}
	else if (*(output)[0] == '\0')
	{
		printf("No output file given! For help use --help parameter.\n");
		return -2;
	}
	else if (*hk == 0)
	{
		printf("Wrong Hk value! For help use --help parameter.\n");
		return -3;
	}
	else if (*he == 0)
	{
		printf("Wrong He value! For help use --help parameter.\n");
		return -4;
	}
	return ret;
}

char* revstr(char* str) {
	int i, len = strlen(str);
	char* out = calloc(len + 1, sizeof(char));
	for (i = len - 1; i >= 0; --i) {
		out[len - 1 - i] = str[i];
	}
	return out;
}

void string_match(char* str, char* substr, int* pos) {
	/*char s_table[256][1];
	char c, *end, *subpos, *revsub;
	int sublen = strlen(substr), i = 0, j;
	end = strchr(substr, '\0')-1;
	*pos = -1;
	for (i = 0; i < 256; i++) {
		c = '\0'+i;
		revsub=revstr(substr);
		subpos = strchr(revsub, c);
		if (!subpos) s_table[i][0] = sublen;
		else s_table[i][0] = subpos-revsub;
		free(revsub);
	}
	i = sublen-1;
	while (i < strlen(str)) {
		for (j = 0; j < sublen; ++j)
			if (str[i - j] != substr[sublen - 1 - j]) break;
		if (j == sublen) { //match
			*pos = i - j + 1; //+1 for for
			return; //Search only the first
		}
		else {
			// Given char plus value    Rollback to given char
			i += s_table[str[i - j]][0] - j > 0 ? s_table[str[i - j]][0] - j : 1;
		}
	}
	return;*/
	char* subpos = strstr(str, substr);
	if ( subpos )
		*pos = subpos - str;
	else *pos = -1;
}

int getwindow(char** hk, char** he, size_t hk_c, size_t he_c, FILE* input, int offset, char prefetch) {
	static long filesize;
	static long fpos = 0;
	static char* minput;
	offset = min(offset, filesize - fpos);
	fpos += offset;
	if (fpos == 0) {
		fseek(input, 0, SEEK_END);
		filesize = ftell(input);
		fseek(input, 0, SEEK_SET);
		minput = malloc(sizeof(char)*filesize + 1);
		*hk = calloc(hk_c + 1, sizeof(char));
		*he = calloc(he_c + 1, sizeof(char));
	}
	else if (offset == 0) return 1;
	if (prefetch) {
		if (fpos == 0) 
			if (!fread(minput, sizeof(char), filesize, input)) {
				return -1;
			}
		minput[filesize] = '\0';
		hk_c = min(hk_c, filesize - fpos);
		he_c = min(he_c, filesize - hk_c - fpos);
		strncpy(*hk, minput + fpos, hk_c);
		strncpy(*he, minput + fpos + hk_c, he_c);
		return 0;
	}
	else {
		fseek(input, fpos, SEEK_SET);
		int hes, hks;
		if ((hks=fread(hk, sizeof(char), hk_c, input)) && (hes=fread(he, sizeof(char), he_c, input))) {
			hk[hks] = '\0';
			he[hes] = '\0';
			return 0;
		}
		else return -1;
	}
}

int getdecwindow(sout* hk, sout* he, size_t hk_c, size_t he_c, FILE* input,long filesize, int offset, char prefetch) {
	static long fpos = 0;
	static sout* minput;
	offset = min(offset, filesize - fpos);
	fpos += offset;
	if (fpos == 0) {
		minput = malloc(sizeof(sout)*filesize);
	}
	else if (offset == 0) return 1;
	if (prefetch) {
		if (fpos == 0)
			if (!fread(minput, sizeof(sout), filesize-1, input)) {
				return -1;
			}
		hk_c = min(hk_c, filesize - fpos);
		he_c = min(he_c, filesize - hk_c - fpos);

		memcpy(hk, minput + fpos, hk_c);
		memcpy(he, minput + fpos + hk_c, he_c);
		return 0;
	}
	else {
		return 0;
	}
}

int compress(int hk_c, int he_c, FILE* input, FILE* output, char prefetch) {
	char* he_temp;
	char* hk, *he;
	int pos, i,j;
	long offset = 0;
	long filesize;
	sout o;
	fseek(input, 0, SEEK_END);
	filesize = ftell(input);
	fseek(input, 0, SEEK_SET);
	if (hk_c + he_c > filesize) {
		hk_c = ((double)filesize * 2.0  + 2.0 )/ 3.0;
		he_c = filesize - hk_c;
	}
	he_temp = calloc(he_c + 1, sizeof(char));
	o.t = hk_c;
	o.h = he_c;
	o.c = '\0';
	j = 0;
	fwrite(&o, sizeof(o), 1, output); //Header
	while (!getwindow(&hk, &he, hk_c, he_c, input, offset, prefetch)) {
		i = -1;
		pos = -1;
		j++;
		while (pos < 0 && i < he_c) {
			strncpy(he_temp, he, he_c - ++i);
			he_temp[he_c - i] = '\0';
			string_match(hk, he_temp, &pos);
		}
		if (pos >= 0) {
			o.t = hk_c - pos;
			o.h = he_c - i;
			o.c = he[he_c - i];
		}
		else {
			o.t = 0;
			o.h = 0;
			o.c = he[he_c - i];
		}
		fwrite(&o, sizeof(o), 1, output);

		offset = he_c - i + 1;
		printf("%.2f\%\r", (((double)offset / (double)filesize) * 100));
	}

}

int decompress(FILE* input, FILE* output,char prefetch) {
	long filesize;
	int hk_c, he_c, offset=0, i, j;
	sout* he, *hk;
	sout* buffer = malloc(sizeof(sout)* 128);
	sout o;
	fseek(input, 0, SEEK_END);
	filesize = ftell(input)/sizeof(sout);
	fseek(input, 0, SEEK_SET);
	fread(&o, sizeof(o), 1, input);
	hk_c = o.t;
	he_c = o.h;
	he = malloc(sizeof(sout)*he_c);
	hk = malloc(sizeof(sout)*hk_c);
	j = 0;
	while (!getdecwindow(hk,he,hk_c,he_c,input,filesize,offset,prefetch)) {
		i = -1;
		j++;
		for (i = 0; i < he_c; ++i) {
				fwrite(&((hk[he[i].t]).c),sizeof(char),he[i].h,output);
				fwrite(&(he[i].c), sizeof(char), 1, output);
		}

		offset = he_c+hk_c;
		printf("%.2f\%\r", (((double)(offset*j) / (double)filesize) * 100));
	}
	free(hk);
	free(he);

}

int main(int ac, char** args) {
	int hk=8, he=4, r;
	char* input = calloc(256, sizeof(char)), *output = calloc(256, sizeof(char));
	FILE *in, *out;
	char prefetch = 0, act=0;
	r = cmdline(ac, args, &hk, &he, &input, &output,&prefetch,&act);
	if (r) return r;
	in = fopen(input, "rb");
	if (!in) {
		printf("Input file not found! For help use --help parameter.\n");
		return -5;
	}
	out = fopen(output, "wb+");
	if (!out) {
		printf("Output file can't be created! Check permissions! For help use --help parameter.\n");
		return -6;
	}
	printf("Hk: %d\n", hk);
	printf("He: %d\n", he);
	if (act == 2) {
		printf("Compressing %s to %s\n", input, output);
		compress(hk, he, in, out, prefetch);
	}
	else if (act == 3) {
		printf("Decompressing %s to %s\n", input, output);
		decompress(in, out, prefetch);
	}
	fclose(in);
	fclose(out);
	return 0;
}